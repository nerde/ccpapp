VCR.configure do |c|
  c.cassette_library_dir = 'spec/vcr'
  c.hook_into :webmock

  c.ignore_request do |request|
    %w(127.0.0.1 localhost).include?(URI(request.uri).host)
  end
end
