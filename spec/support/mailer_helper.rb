module MailerHelper
  def last_email
    ActionMailer::Base.deliveries.last
  end

  def sent_emails
    ActionMailer::Base.deliveries
  end

  def clear_emails
    ActionMailer::Base.deliveries = []
  end
end

RSpec.configure do |config|
  config.include MailerHelper
end
