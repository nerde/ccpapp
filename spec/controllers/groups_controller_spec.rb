require 'rails_helper'

describe GroupsController, type: :controller do

  let(:valid_attributes) do
    {
      name: 'Baseball Bats',
      products_attributes: [
        {
          url_or_asin: 'https://www.amazon.com/Louisville-Slugger-Omaha-BBCOR-Baseball/dp/B01JP6E9HY/ref=sr_1_6?ie=UTF8&qid=1501170697&sr=8-6&keywords=baseball+bats'
        }
      ]
    }
  end

  let(:invalid_attributes) do
    { name: nil }
  end

  let(:valid_session) { {} }

  describe 'GET #index' do
    it 'returns a success response' do
      group = Group.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      group = Group.create! valid_attributes
      get :show, params: {id: group.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end

    context 'when user has reached groups limit' do
      it 'denies access' do
        10.times { create :group }

        get :new

        expect(response).to redirect_to(groups_path)
      end
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      group = Group.create! valid_attributes
      get :edit, params: {id: group.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Group' do
        expect {
          post :create, params: {group: valid_attributes}, session: valid_session
        }.to change(Group, :count).by(1)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the \'new\' template)' do
        post :create, params: {group: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end

    context 'when user has reached groups limit' do
      it 'denies access' do
        10.times { create :group }

        expect do
          post :create, params: { group: valid_attributes }
        end.to_not change(Group, :count)

        expect(response).to redirect_to(groups_path)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        {
          name: 'Updated Group'
        }
      end

      it 'updates the requested group' do
        group = Group.create! valid_attributes
        patch :update, params: {id: group.to_param, group: new_attributes}, session: valid_session
        group.reload
        expect(group.name).to eq('Updated Group')
      end

      it 'redirects to the group' do
        group = Group.create! valid_attributes
        patch :update, params: {id: group.to_param, group: valid_attributes}, session: valid_session
        expect(response).to redirect_to(group)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the \'edit\' template)' do
        group = Group.create! valid_attributes
        patch :update, params: {id: group.to_param, group: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested group' do
      group = Group.create! valid_attributes
      expect {
        delete :destroy, params: {id: group.to_param}, session: valid_session
      }.to change(Group, :count).by(-1)
    end

    it 'redirects to the groups list' do
      group = Group.create! valid_attributes
      delete :destroy, params: {id: group.to_param}, session: valid_session
      expect(response).to redirect_to(groups_url)
    end
  end
end
