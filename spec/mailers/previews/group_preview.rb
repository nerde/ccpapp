# Preview all emails at http://localhost:3000/rails/mailers/group
class GroupPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/group/diff
  def diff
    GroupMailer.diff(Group.maximum(:id))
  end
end
