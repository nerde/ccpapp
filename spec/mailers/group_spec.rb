require 'rails_helper'

describe GroupMailer, type: :mailer do
  describe 'diff' do
    let(:group) { create :group }
    let(:product) { create :product, group: group }
    let(:mail) { GroupMailer.diff(group.id) }

    before do
      today = Date.today
      create :snapshot, product: product, taken_on: today.yesterday, title: 'AB'
      create :snapshot, product: product, taken_on: today, title: 'CD'
    end

    it 'lists differences between snapshots in a group' do
      expect(mail.body.encoded).to match(/AB.*CD/m)
    end
  end
end
