require 'rails_helper'

describe SnapshotTakerJob, type: :job do
  let(:job) { SnapshotTakerJob.new }

  it 'generates snapshots for all products in a group' do
    clear_emails
    group = create :group
    create :product, group: group, url_or_asin: 'B01K8B5BYU'
    create :product, group: group, url_or_asin: 'B01JP6E9HY'

    VCR.use_cassette 'snapshot_taker_job' do
      expect do
        job.perform group.id
      end.to change(group.snapshots, :count).by(2)
      expect(sent_emails.size).to eq(0)
    end
  end

  context 'when there has a previous snapshot' do
    it 'sends email with differences' do
      clear_emails
      group = create :group
      p1 = create :product, group: group, url_or_asin: 'B01K8B5BYU'
      p2 = create :product, group: group, url_or_asin: 'B01JP6E9HY'
      create :snapshot, product: p1, taken_on: Date.yesterday
      create :snapshot, product: p2, taken_on: Date.yesterday

      VCR.use_cassette 'snapshot_taker_job' do
        job.perform group.id
      end

      expect(sent_emails.size).to eq(1)
    end
  end
end
