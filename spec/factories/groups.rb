FactoryGirl.define do
  factory :group do
    sequence :name, 'Group 000'
  end
end
