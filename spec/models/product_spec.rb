require 'rails_helper'

describe Product, type: :model do
  describe '#take_snapshot!' do
    let(:url_or_asin) { 'https://www.amazon.com/gp/product/B01K86V0CM' }
    let(:product) { create :product, url_or_asin: url_or_asin }

    it 'takes snapshots from Amazon website' do
      VCR.use_cassette 'amazon/B01K86V0CM' do
        expect do
          product.take_snapshot!
        end.to change(product.snapshots, :count).by(1)

        snapshot = product.snapshots.last
        expect(snapshot.title).to eq('Louisville Slugger Genuine Series 3X Ash '\
                                     'Mixed Baseball Bat')
        expect(snapshot.taken_on).to eq(Date.today)
        expect(snapshot.min_price).to eq(29.95)
        expect(snapshot.max_price).to eq(51.28)
        expect(snapshot.reviews_count).to eq(49)
        expect(snapshot.rank).to eq(13_647)
        expect(snapshot.features).to eq([
          'Louisville Slugger Genuine S3X Mixed Ash Wood Baseball Bat Louisville'\
          ' Slugger\'s adult wood bats are pulled from their original production'\
          ' line for some minor flaw that will not affect the bat\'s performance'\
          '. These small production errors mean deep savings on superior bats '\
          'ideal for practice, batting cages or even games. Bat Specifications '\
          'Wood: Series 3X Ash Finishes: Natural Finish & Black Finish Turning '\
          'Model: Mixed Cupped: Yes Available Sizes: $size$'
        ])
        expect(snapshot.images).to eq([
          'https://images-na.ssl-images-amazon.com/images/I/21sM8hUUgAL._SS40_.jpg',
          'https://images-na.ssl-images-amazon.com/images/I/21NsdKFtA0L._SS40_.jpg'
        ])
        expect(snapshot.stock).to eq(999)
        expect(product.reload.last_snapshot).to eq(snapshot)
      end
    end

    it 'replaces other snapshots for the same date' do
      snapshot = create :snapshot, product: product, taken_on: Date.today
      VCR.use_cassette 'amazon/B01K86V0CM' do
        expect do
          product.take_snapshot!
        end.to_not change(product.snapshots, :count)
        expect(snapshot.reload.min_price).to eq(29.95)
        expect(product.reload.last_snapshot).to eq(snapshot)
      end
    end

    context 'when using ASIN' do
      let(:url_or_asin) { 'B00L9A95T2' }

      it 'detects URL and works the same' do
        VCR.use_cassette 'amazon/B00L9A95T2' do
          expect do
            product.take_snapshot!
          end.to change(product.snapshots, :count).by(1)

          snapshot = product.snapshots.last
          expect(snapshot.title).to eq('Marucci Cat 6 BBCOR Baseball Bat')
          expect(snapshot.taken_on).to eq(Date.today)
          expect(snapshot.min_price).to eq(159.5)
          expect(snapshot.max_price).to eq(249.99)
          expect(snapshot.reviews_count).to eq(56)
          expect(snapshot.rank).to eq(42_945)
          expect(snapshot.features).to eq([
            'One-piece alloy construction',
            'Professionally-inspired extended barrel profile',
            'Ring-free barrel technology',
            'Patented anti-vibration knob',
            'BBCOR Certified'
          ])
          expect(snapshot.images).to eq([
            'https://images-na.ssl-images-amazon.com/images/I/21xCHLcJD8L._SS40_.jpg',
            'https://images-na.ssl-images-amazon.com/images/I/21ejdQPTe2L._SS40_.jpg'
          ])
          expect(snapshot.stock).to eq(3)
          expect(product.reload.last_snapshot).to eq(snapshot)
        end
      end
    end
  end
end
