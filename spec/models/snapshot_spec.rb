require 'rails_helper'

describe Snapshot, type: :model do
  it 'generates a list of changes' do
    sn1 = build :snapshot, min_price: 1.0, max_price: 2.0, images: %w(im1 im2),
                           title: 'A', reviews_count: 3, stock: 5, rank: 8
    sn2 = sn1.dup
    sn2.assign_attributes(min_price: 1.5, max_price: 2.2, images: %w(im1 im3),
                          title: 'B', reviews_count: 13, stock: 21, rank: 34,
                          features: %w(f1))

    expect(sn1.diff_to(sn2)).to eq(
      title: %w(A B),
      reviews_count: [3, 13],
      stock: [5, 21],
      rank: [8, 34],
      min_price: [1.0, 1.5],
      max_price: [2.0, 2.2],
      features: { removed: [], added: %w(f1) },
      images: { removed: %w(im2), added: %w(im3) }
    )
  end
end
