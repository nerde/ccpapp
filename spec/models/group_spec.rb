require 'rails_helper'

describe Group, type: :model do
  it 'allows up to 8 products in the group' do
    group = build :group

    8.times { group.products.build }

    expect(group).to be_valid

    group.products.build

    expect(group).to_not be_valid
    expect(group.errors[:products_count]).to_not be_empty

    group.products.last.mark_for_destruction

    expect(group).to be_valid
  end
end
