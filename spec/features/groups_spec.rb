require 'rails_helper'

feature 'Groups', type: :feature do
  scenario 'user adds a new group to track' do
    visit root_path

    click_on 'Track my Competitors'
    fill_in 'Name', with: 'Baseball Bats'
    fill_in 'Product #1', with: 'Baseball Batshttps://www.amazon.com/Louisville-Slugger-Genuine-Baseball-Natural/dp/B01K8B5BYU/ref=sr_1_3?ie=UTF8&qid=1501170697&sr=8-3&keywords=baseball+bats'
    fill_in 'Product #2', with: 'https://www.amazon.com/Louisville-Slugger-Omaha-BBCOR-Baseball/dp/B01JP6E9HY/ref=sr_1_6?ie=UTF8&qid=1501170697&sr=8-6&keywords=baseball+bats'
    click_on 'Start Tracking'

    expect(page).to have_content('Baseball Bats')
    expect(page).to have_content('2 products')
  end

  scenario 'user has reached groups limit' do
    10.times { create :group }
    visit root_path

    expect(page).to_not have_link('Track my Competitors')

    visit groups_path

    expect(page).to_not have_link('Track my Competitors')
  end
end
