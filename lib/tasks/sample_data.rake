namespace :sample_data do
  task :generate do
    if Group.any?
      puts 'Database already has data'
      next
    end

    group = Group.create!(name: 'Baseball Bats')
    products = group.products
    products.create!(url: 'https://www.amazon.com/Louisville-Slugger-Genuine-'\
                          'Baseball-Natural/dp/B01K8B5BYU/ref=sr_1_3?ie=UTF8&'\
                          'qid=1501170697&sr=8-3&keywords=baseball+bats')
    products.create!(url: 'https://www.amazon.com/Louisville-Slugger-Omaha-BBC'\
                          'OR-Baseball/dp/B01JP6E9HY/ref=sr_1_6?ie=UTF8&qid='\
                          '1501170697&sr=8-6&keywords=baseball+bats')
    products.create!(url: 'https://www.amazon.com/Rawlings-YBRR11-Raptor-Youth'\
                          '-Barrel/dp/B011BX2C50/ref=sr_1_7?ie=UTF8&qid='\
                          '1501170697&sr=8-7&keywords=baseball+bats')

    names = ['Bat XYZ', 'Bat ABC', 'Bat 123', 'Bat Man']
    images = [
      'https://images-na.ssl-images-amazon.com/images/I/21xCHLcJD8L._SS40_.jpg',
      'https://images-na.ssl-images-amazon.com/images/I/21ejdQPTe2L._SS40_.jpg',
      'https://images-na.ssl-images-amazon.com/images/I/21sLMjWaKlL._SS40_.jpg'
    ]
    features = ['Durable', 'Ring-free', 'Patented']
    group.products.each do |product|
      (Date.yesterday.yesterday..Date.today).each do |date|
        min_price = rand(0..50.0).round(2)
        max_price = rand(min_price..80.0).round(2)

        product.snapshots.create!(taken_on: date,
                                  title: names.sample,
                                  min_price: min_price,
                                  max_price: max_price,
                                  images: images.sample(2),
                                  features: features.sample(2),
                                  reviews_count: rand(10..200),
                                  stock: rand(0..999),
                                  rank: rand(500..5_000))
      end
    end
  end
end
