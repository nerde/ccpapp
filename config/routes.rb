Rails.application.routes.draw do
  resources :groups do
    put :check, on: :member
  end

  root to: 'home#index'
end
