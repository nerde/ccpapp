# Cash Cow Pro - Dev Test

This app was developed as a test app to apply for a position in Cash Cow Pro.

The app's goals are:

* Allow users to manage groups of competitors selling on Amazon;
* Notify users when something changed in his competitors' products.

## Dependencies

In order to get this app running, you'll need:

* Ruby 2.3.4;
* Redis;
* MySQL Database.

## Installation

* Clone this project (if needed)
* Go into app's folder
* Make sure Bundler is installed: `gem install bundler`
* Install dependencies: `bundle`
* Make sure the DB configuration in `config/database.yml` is valid for you
* Setup database: `rake db:setup`
* Make sure Redis is running
* Start the app: `foreman start`

If your database is empty, you can generate test data by running
`rake sample_data:generate`

Point your browser to `localhost:3000`.

## How it Works

Users can add groups of products. Products can be added via URL or ASIN.
Every day, the system will check products' pages in Amazon and save a new
`Snapshot` of each product. A `Snapshot` contains all the trackable details:

* Title;
* Price;
* Images;
* Features;
* Number of Reviews;
* Stock;
* Seller Rank;

Each snapshot also has a date field that identifies the day that snapshot was
taken. If it's different from the snapshot taken the day before, an e-mail is
sent notifying the user about the changes. All the changes related to products
in a group go in a single e-mail.

If no changes were detected, then we send an e-mail telling the user nothing
has changed.

Check `doc/entities.png` to see how the models relate to each other.

## Seeing it in Action

With test data in place, you can go to Groups page and click on `Run Check`.
This will trigger a background job that simulates the daily check. This link
is only available in development mode.

## Tests

To run tests, just execute `rspec`.

## Limits

The system only allows 8 products per group and allows up to 10 groups of
products.
