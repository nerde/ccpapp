class SnapshotsComparator
  attr_reader :previous, :current

  def initialize(previous, current)
    @previous = previous
    @current = current
  end

  def as_hash
    result = {}
    each_difference do |field, old_value, new_value|
      result[field] = field_diff(field, old_value, new_value)
    end
    result
  end

  def each_difference
    Snapshot::TRACKED_FIELDS.map do |field|
      old_value = previous[field]
      new_value = current[field]
      next if old_value == new_value

      yield(field, old_value, new_value)
    end
  end

  private

  def field_diff(field, old_value, new_value)
    return [old_value, new_value] unless array_field?(field)

    array_diff(old_value, new_value)
  end

  def array_field?(field)
    Snapshot::ARRAY_FIELDS.include?(field)
  end

  def array_diff(old_value, new_value)
    {
      removed: Array(old_value) - Array(new_value),
      added:   Array(new_value) - Array(old_value)
    }
  end
end
