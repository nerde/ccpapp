class AmazonProductGateway
  def initialize(product)
    @product = product
  end

  def title
    product_page.search('#productTitle').first&.text&.strip
  end

  def min_price
    price_range.split('-').first&.strip&.gsub('$', '')&.to_d
  end

  def max_price
    price_range.split('-').last&.strip&.gsub('$', '')&.to_d
  end

  def price_range
    @price_range ||= product_page.search('#priceblock_ourprice').first&.text
  end

  def images
    product_page.search('#altImages li.item img').map do |img|
      img.attribute('src').value
    end
  end

  def reviews_count
    product_page.search('#acrCustomerReviewText').first&.text.to_i
  end

  def features
    product_page.search('#feature-bullets li').map(&:text).map(&:strip)
  end

  def rank
    product_page.search('#SalesRank').text[/[\d,]+/].delete(',').to_i
  end

  def stock
    form = cart_page.form_with(action: '/gp/aws/cart/add.html')
    input = agent.submit(form).search('input[name=quantityBox]').first
    return 0 unless input
    input['value'].to_i
  end

  def product_page
    @product_page ||= agent.get(product_url)
  end

  def cart_page
    @cart_page ||= agent.get(cart_url)
  end

  def cart_url
    "https://www.amazon.com/gp/aws/cart/add.html?ASIN.1=#{asin}&Quantity.1=999"
  end

  def agent
    @agent ||= Mechanize.new.tap do |agent|
      agent.user_agent = Mechanize::AGENT_ALIASES['Mac Firefox']
    end
  end

  private

  def product_url
    @product.safe_url
  end

  def asin
    @product.safe_asin
  end
end
