class GroupMailer < ApplicationMailer
  def diff(group_id, date = Date.today)
    @group = Group.find_by(id: group_id)
    return unless @group

    @snapshots = @group.snapshots.where(taken_on: date).preload(:product).to_a
    @previous_snapshots = @group.snapshots.where(taken_on: date.yesterday).to_a

    return if @snapshots.empty? || @previous_snapshots.empty?

    @diffs = @snapshots.map do |snapshot|
      previous = @previous_snapshots.detect do |sn|
        sn.product_id == snapshot.product_id
      end
      diff = previous.diff_to(snapshot)
      next unless diff.any?
      [snapshot, diff]
    end

    @date = date

    if @diffs.any?
      subject = "#{@group.name}: see what your competitors have been up to!"
    else
      subject = "#{@group.name}: no changes detected!"
    end

    mail to: 'user@example.org', subject: subject
  end
end
