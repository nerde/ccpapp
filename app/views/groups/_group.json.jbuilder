json.extract! group, :id, :name, :products_count, :created_at, :updated_at
json.url group_url(group, format: :json)
