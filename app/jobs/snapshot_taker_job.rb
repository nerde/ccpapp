class SnapshotTakerJob < ApplicationJob
  queue_as :default

  def perform(group_id)
    @group = Group.find_by(id: group_id)
    return unless @group

    @group.products.each(&:take_snapshot!)
    GroupMailer.diff(group_id).deliver_now
  end
end
