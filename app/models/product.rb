class Product < ApplicationRecord
  belongs_to :group, counter_cache: true
  belongs_to :last_snapshot, class_name: 'Snapshot', optional: true
  has_many :snapshots, dependent: :destroy

  def url_or_asin
    url || asin
  end

  def url_or_asin=(url_or_asin)
    if url_or_asin.include?('/')
      self.url = url_or_asin
    else
      self.asin = url_or_asin
    end
  end

  def take_snapshot!
    snapshots.find_or_initialize_by(taken_on: Date.today).tap do |snap|
      snap.import_from(gateway)
      snap.transaction do
        snap.save!
        refresh_last_snapshot!
      end
    end
  end

  def gateway
    @gateway ||= AmazonProductGateway.new(self)
  end

  def refresh_last_snapshot!
    update!(last_snapshot: snapshots.order(:taken_on).last)
  end

  def safe_url
    return url if url.present?
    "https://www.amazon.com/dp/#{asin}"
  end

  def safe_asin
    return asin if asin.present?
    url[/\/(dp|product)\/(\w+)/, 2]
  end
end
