class Group < ApplicationRecord
  has_many :products, dependent: :destroy
  has_many :snapshots, through: :products

  validates :name, presence: true
  validates :products_count, numericality: { less_than: 9 }

  url_or_asin_is_blank = ->(att) { att[:url_or_asin].blank? }
  accepts_nested_attributes_for :products, allow_destroy: true,
                                           reject_if: url_or_asin_is_blank

  def build_empty_products
    (8 - products.size).times { products.build }
  end

  def products_count
    products.reject(&:marked_for_destruction?).size
  end
end
