class Snapshot < ApplicationRecord
  TRACKED_FIELDS = %i(title min_price max_price images features reviews_count
                      stock rank)
  ARRAY_FIELDS = %i(images features)

  belongs_to :product

  ARRAY_FIELDS.each { |field| serialize field, Array }

  def diff_to(other)
    SnapshotsComparator.new(self, other).as_hash
  end

  def import_from(source)
    Snapshot::TRACKED_FIELDS.each do |field|
      self[field] = source.public_send(field)
    end
  end
end
