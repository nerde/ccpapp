class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :can_track_competitors?

  private

  def can_track_competitors?
    Group.count < 10
  end
end
