class GroupsController < ApplicationController
  before_action :allow_creation, only: %i(new create)
  before_action :set_group, only: %i(show edit update destroy check)

  def index
    @groups = Group.all
  end

  def show
  end

  def new
    @group = Group.new
    @group.build_empty_products
  end

  def edit
    @group.build_empty_products
  end

  def create
    @group = Group.new(group_params)

    respond_to do |format|
      if @group.save
        format.html do
          redirect_to action: :index, notice: 'Group was successfully created.'
        end
        format.json { render :show, status: :created, location: @group }
      else
        format.html { render :new }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html do
          redirect_to @group, notice: 'Group was successfully updated.'
        end
        format.json { render :show, status: :ok, location: @group }
      else
        format.html { render :edit }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @group.destroy
    respond_to do |format|
      format.html do
        redirect_to groups_url, notice: 'Group was successfully destroyed.'
      end
      format.json { head :no_content }
    end
  end

  def check
    unless Rails.env.development?
      redirect_to root_path
      return
    end

    SnapshotTakerJob.perform_later(@group.id)
    redirect_to groups_path, notice: 'Check started'
  end

  private

  def set_group
    @group = Group.find(params[:id])
  end

  def group_params
    params.require(:group).permit(
      :name, products_attributes: %i(id url_or_asin _destroy)
    )
  end

  def allow_creation
    return if can_track_competitors?
    redirect_to(action: :index)
  end
end
