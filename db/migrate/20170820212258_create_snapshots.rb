class CreateSnapshots < ActiveRecord::Migration[5.1]
  def change
    create_table :snapshots do |t|
      t.belongs_to :product, foreign_key: true
      t.date :taken_on
      t.string :title
      t.decimal :min_price, precision: 10, scale: 2
      t.decimal :max_price, precision: 10, scale: 2
      t.text :images
      t.text :features
      t.integer :reviews_count
      t.integer :stock
      t.integer :rank

      t.timestamps
    end
  end
end
