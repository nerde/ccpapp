class AddLastSnapshotToProducts < ActiveRecord::Migration[5.1]
  def change
    add_reference :products, :last_snapshot
  end
end
