class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.belongs_to :group, foreign_key: true
      t.string :name
      t.string :url
      t.string :asin

      t.timestamps
    end
  end
end
